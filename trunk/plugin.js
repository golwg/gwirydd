/* ===================================================================

Rhyngwyneb HTML i’r API Cysill a ddarperir gan Brifysgol Bangor.
Manylion, a sut i gael allwedd API, yma: https://api.techiaith.org/cy/

Datblygwyd gan Iwan Standley ar gyfer cynllun cyhoeddi cymunedol Bro360.

Hawlfraint: Golwg Cyf <gwefan@golwg.com> https://golwg.cymru

Mae’r ffeil hwn yn ategyn ar gyfer TinyMCE sy’n llwytho’r gwirydd i’r golygydd.
Mae modd ei ddefnyddio gyda TinyMCE yn annibynnol o WordPress hefyd.

=================================================================== */

'use strict';

(function() {

	tinymce.create('tinymce.plugins.gwirydd', {

		init: function(editor, url) {
			var gwirydd, botwm, rhif;
			try {
				new Function('(y => y)'); // cadarnhau ES6
				gwirydd = new Gwirydd(editor.settings.gwirydd.allwedd || window.gwirydd.allwedd);
			}
			catch (e) {
				console.warn('Dydi’r gwirydd ddim yn gweithio yn y porydd hwn.');
			}

			var gwirioHandler = function(e) {
				if (!botwm) return;
				botwm.disabled(true);
				botwm.$el[0].classList.add('gwirydd-brysur');
			}

			var gwallHandler = function(e) {
				if (!botwm) return;
				botwm.disabled(false);
				if (e.detail === 0) {
					if (rhif) {
						rhif.remove();
						rhif = false;
					}
					else {
						alert('Wedi gorffen gwirio.');
					}
					botwm.$el[0].classList.remove('gwirydd-rhifau','gwirydd-brysur');
					botwm.active(false);
				}
				else {
					if (!rhif) {
						botwm.active(true);
						botwm.$el[0].classList.remove('gwirydd-brysur');
						botwm.$el[0].classList.add('gwirydd-rhifau');
						rhif = document.createElement('b');
						botwm.$el[0].querySelector('button').appendChild(rhif);

					}
					rhif.textContent = e.detail;
				}
			};

			editor.addButton( 'gwirydd', {
				title: 'Gwirydd Sillafu',
				tooltip: gwirydd ?
					'Dangos awgrymiadau sillafu a gramadeg Cymraeg.' :
					'Dydi’r gwirydd ddim ar gael ar hyn o bryd.',
				icon: 'gwirydd gwirydd-eicon',
				onclick: function(e) {
					if (!editor.settings.gwirydd && !window.gwirydd) {
						alert('Mae angen rhoi’r allwedd API yn y Gosodiadau.');
						return;
					}
					var body = editor.getBody();
					if (this.active()) {
						this.active(false);
						gwirydd.clirio();
					}
					else {
						gwirydd.addListener(body, 'gwirio', gwirioHandler);
						gwirydd.addListener(body, 'gwallau', gwallHandler);
						var stad = editor.isDirty();
						gwirydd.gwirio(body);
						editor.setDirty(stad);
					}
				},
				onPostRender: function() {
					botwm = this;
					if (!gwirydd) this.disabled(true);
				},
			});

			editor.on( 'PreProcess', function(e) {
				for (var el of e.node.querySelectorAll('.gwall-gwirio')) {
					el.replaceWith(...el.childNodes);
				}
				var g = e.node.querySelector('#gwirio');
				if (g) g.remove();
			});

			editor.on( 'hide', function(e) {
				if (e.type == 'hide' && editor.isHidden()) {
					gwirydd.clirio();
				}
			});

			editor.addButton('acenion', {
				title: 'Acenion Cymraeg',
				tooltip: 'Teipio toeon bach ac acenion eraill.',
				icon: 'acenion gwirydd-eicon',
				onclick: function(e) {
					ffenestAcenion(editor);
				},
			});

			var v = this.getInfo().version;

			// CSS i’r blwch golygu
			editor.contentCSS.push(url + '/golygydd.css?v=' + v);

			// CSS i’r golygydd
			var link = document.createElement('link');
			link.href = url + '/gwirydd.css?v=' + v;
			link.rel = 'stylesheet';
			document.head.appendChild(link);

		},
		createControl: function(n, cm) {
			return null;
		},
		getInfo: function() {
			return {
				longname: 'Gwirydd Sillafu',
				author: 'Iwan Stanno',
				authorurl: 'https://stanno.cymru',
				infourl: '',
				version: '1.3'
			};
		}
	});

	function ffenestAcenion(editor) {
		var win;
		var acenion = 'ˆ¨´`˜';
		var chars = ['âêîôûŵŷ', 'äëïöüẅÿ', 'áéíóúẃý', 'àèìòùẁỳ', 'ãñõåøçæ'];
		var i = 0;
		var tabl = '<table class="gwirydd-acenion">';
		for (var r of chars) {
			var tr1 = '<tr class="' + (i++ ? 'cuddio ' : '') + 'rhes colofn-' + i + '">',
				tr2 = tr1;
			for (var c of r.split('')) {
				tr1 += '<td><div>' + c + '</div></td>';
				tr2 += '<td><div>' + c.toUpperCase() + '</div></td>';
			}
			tabl += tr1 + '</tr>' + tr2 + '</tr>';
		}
		tabl += '</table><table class="gwirydd-acenion"><tr>';
		i = 0;
		for (var a of acenion.split('')) {
			tabl += '<th' + (i++ ? '' : ' class="dangos"') + ' data-colofn="' + i + '"><div>'+a+'</div></th>';
		}
		tabl += '</tr></table>';
		var panel = {
			type: 'container',
			html: tabl,
			onclick: function (e) {
				var cell = e.target.closest('td,th');
				var ct = e.currentTarget;
				if (!cell) {
					return;
				} else if (cell.nodeName == 'TD') {
					editor.insertContent(cell.textContent);
					win.close();
				} else if (cell.classList.contains('dangos')) {
					return;
				} else {
					var i = cell.dataset.colofn;
					for (var el of ct.querySelectorAll('.rhes:not(.colofn-' + i + ')')) {
						el.classList.add('cuddio');
					}
					for (var el of ct.querySelectorAll('.colofn-' + i)) {
						el.classList.remove('cuddio');
					}
					for (var el of ct.querySelectorAll('.dangos')) {
						el.classList.remove('dangos');
					}
					cell.classList.add('dangos');
				}
			},
		};
		win = editor.windowManager.open({
			title: 'Mewnosod acenion',
			spacing: 10,
			padding: 10,
			items: [panel],
			buttons: [{
				text: 'Cau',
				onclick: function() {
					win.close();
				}
			}]
		});
	};

	tinymce.PluginManager.add('gwirydd', tinymce.plugins.gwirydd);

})();
