/* ===================================================================

Rhyngwyneb HTML i’r API Cysill a ddarperir gan Brifysgol Bangor.
Manylion, a sut i gael allwedd API, yma: https://api.techiaith.org/cy/

Datblygwyd gan Iwan Standley ar gyfer cynllun cyhoeddi cymunedol Bro360.

Hawlfraint: Golwg Cyf <gwefan@golwg.com> https://golwg.cymru

Dyma’r ffeil sy’n gwneud y gwaith o gyfathrebu gyda’r API a chyflwyno’r
canlyniadau. Dylai hwn fod yn ddigon cyffredinol i’w ddefnyddio gydag
unrhyw ddarn o HTML, heb fod angen TinyMCE na WordPress.

=================================================================== */

class Gwirydd {

	constructor(apiKey) {
		this.apiKey = apiKey;
		this.apiUrl = 'https://api.techiaith.org/cysill/v1/';
		this.maxRequest = 20480;
		this.lang = 'cy';
		this.data = [];
		this.dict = this.darllen();
		this.ignore = [];
		this.handlers = [];
		this.open = false;
		this.timer = false;
		this.edit = false;
	}

	gosod(editor) {
		if (!editor.isContentEditable) {
			const daliwr = document.createElement('div');
			daliwr.className = 'gwirio-daliwr';
			const gorchudd = document.createElement('div');
			gorchudd.className = 'gwirio-gorchudd';
			const editorStyle = window.getComputedStyle(editor);
			for (const prop of ['margin', 'border-width', 'padding', 'font-size', 'font-family', 'font-weight', 'font-style', 'line-height']) {
				gorchudd.style.setProperty(prop, editorStyle[prop]);
			}
			//gorchudd.style.width = editor.offsetWidth + 'px';
			gorchudd.innerText = editor.value;
			editor.insertAdjacentElement('afterend', daliwr);
			daliwr.appendChild(editor);
			daliwr.appendChild(gorchudd);
			this.targed = editor;
			this.edit = gorchudd;
			return;
		}
		this.edit = editor;
		this.menu = this.creuDewislen(this.edit);
		this.nodes = this.edit.getElementsByClassName('gwall-gwirio');

		this.addListener(this.edit, 'click', e => {
			const el = e.target.closest('.gwall-gwirio');
			if (el && el != this.open) {
				e.preventDefault();
				this.dangos(el);
				return;
			}
			else if (e.target.tagName == 'LI' && e.target.closest('#gwirio')) {
				e.preventDefault();
				switch (e.target.id) {
					case 'gwirio-nol':
						this.cynt();
						return;
					case 'gwirio-nesaf':
						this.nesaf();
						return;
					case 'gwirio-dysgu':
						this.dysgu(this.open.textContent);
						break;
					case 'gwirio-neidio':
						this.anwybyddu(this.open.textContent);
						break;
					default:
						this.cyfnewid(this.open, e.target);
				}
			}
			this.cau();
		});

		this.addListener(this.edit, 'keydown', e => {
			if (!this.open) return;
			const el = this.open;
			let li, d;
			switch (e.keyCode) {
				case 13://↵
					li = this.ol.querySelector('.dewis');
					break;
				case 27:
					this.cau();
					break;
				case 37://←
					this.cynt();
					break;
				case 38: //↑
					d = this.ol.querySelector('.dewis');
					d && d.classList.remove('dewis');
					d = (d && d.previousSibling) || this.ol.children[this.ol.children-1];
					d && d.classList.add('dewis');
					break;
				case 39://→
					this.nesaf();
					break;
				case 40: //↓
					d = this.ol.querySelector('.dewis');
					d && d.classList.remove('dewis');
					d = (d && d.nextSibling) || this.ol.children[0];
					d && d.classList.add('dewis');
					break;
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
				case 54:
				case 55:
				case 56:
				case 57:
					li = this.ol.children[e.keyCode - 49];
					break;
				case 65:
					this.nesaf();
					this.anwybyddu(el.textContent);
					break;
				case 68:
					this.nesaf();
					this.dysgu(el.textContent);
					break;
				default:
					return;
			}
			e.preventDefault();
			if (li) {
				this.nesaf();
				this.cyfnewid(el, li);
			}
		});

		this.addListener(this.edit, 'input', e => {
			this.cau();
			if (!this.timer) this.timer = setTimeout(() => this.arNewid(), 1000);
		});

		this.addListener(window, 'resize', () => this.cau());

	}

	addListener(obj, type, handler) {
		obj.addEventListener(type, handler);
		this.handlers.push({obj, type, handler});
	}

	cynt() {
		const n = this.nodes[(this.nodes.length + this.nodeIndex(this.open) - 1) % this.nodes.length];
		n == this.open ? this.cau() : this.dangos(n);
	}

	nesaf() {
		const n = this.nodes[(this.nodeIndex(this.open) + 1) % this.nodes.length];
		n == this.open ? this.cau() : this.dangos(n);
	}

	clirio() {
		if (this.nodes) {
			while (this.nodes.length) {
				this.nodes[0].replaceWith(...this.nodes[0].childNodes);
			}
		}
		this.diweddaru();
		this.glanhau();
	}

	glanhau() {
		let h;
		while (h = this.handlers.pop()) {
			h.obj.removeEventListener(h.type, h.handler);
		}
		if (this.edit) {
			this.edit.classList.remove('gwirydd-gwirio', 'gwirydd-agored');
			this.edit.removeAttribute('spellcheck');
		}
		this.menu && this.menu.remove();
		this.menu = this.edit = false;
	}

	arNewid() {
		for (let n of this.nodes) {
			if (this.getData(n).text !== n.textContent) this.cuddio(n);
		}
		this.timer = false;
	}

	cyfnewid(el, t) {
		el.replaceWith(t.firstChild.cloneNode());
		if (!this.diweddaru()) this.gwirioEto ? this.gwirio() : this.glanhau();
	}

	cuddio(el) {
		el.replaceWith(...el.childNodes);
		if (!this.diweddaru()) this.gwirioEto ? this.gwirio() : this.glanhau();
	}

	cuddioPob(t) {
		for (let n of this.nodes) {
			if (t == this.normaleiddio(n.textContent)) {
				this.cuddio(n);
			}
		}
	}

	nodeIndex(el) {
		let i = 0;
		for (let n of this.nodes) {
			if (n == el) return i;
			i++;
		}
	}

	darllen() {
		try {
			const g = window.localStorage.getItem('gwirydd');
			return g ? g.split("\n") : [];
		}
		catch(e) {
			return [];
		}
	}

	ysgrifennu() {
		try {
			window.localStorage.setItem('gwirydd', this.dict.join("\n"));
			return true;
		}
		catch(e) {
			return false;
		}
	}

	dysgu(t) {
		t = this.normaleiddio(t);
		if (this.dict.indexOf(t) == -1) {
			this.dict.push(t);
			this.ysgrifennu();
			this.geiriau = false;
			this.edit && this.edit.dispatchEvent(new CustomEvent('cofio', { detail: t }));
		}
		this.cuddioPob(t);
	}

	anghofio(t) {
		t = this.normaleiddio(t);
		const i = this.dict.indexOf(t);
		if (i != -1) {
			this.dict.splice(i, 1);
			this.ysgrifennu();
			this.geiriau = false;
			this.edit && this.edit.dispatchEvent(new CustomEvent('anghofio', { detail: t }));
		}
	}

	anwybyddu(t) {
		t = this.normaleiddio(t);
		if (this.ignore.indexOf(t) == -1) {
			this.ignore.push(t);
			this.geiriau = false;
		}
		this.cuddioPob(t);
	}

	anwybyddwyd(t) {
		if (!this.geiriau) this.geiriau = this.dict.concat(this.ignore);
		return this.geiriau.indexOf(this.normaleiddio(t)) !== -1;
	}

	diweddaru(){
		const l = this.nodes ? this.nodes.length : 0;
		this.edit && this.edit.dispatchEvent(new CustomEvent('gwallau', { detail: l }));
		return l;
	}

	normaleiddio(t) {
		return t.replace(/\s+/g, ' ').replace(/^ /, '').replace(/ $/, '').toLowerCase();
	}

	atalnodi(t) {
		return t.replace(/ "/g, ' “').replace(/"/g, '”').replace(/ '/g, ' ‘').replace(/'/g, '’');
	}

	setData(el, data) {
		if (!el._rhif) el._rhif = this.data.length;
		this.data[el._rhif] = data;
	}

	getData(el) {
		return this.data[el._rhif];
	}

	resetData() {
		this.data = [];
	}

	dewis(n){
		const s = window.getSelection();
		const r = document.createRange();
		r.selectNodeContents(n);
		s.removeAllRanges();
		s.addRange(r);
		return r;
	}

	creuDewislen(el) {
		if (!this.menu) {
			const d = document.createElement('div'),
				n = document.createElement('div'),
				o = document.createElement('ol'),
				b = document.createElement('ul');
			d.id='gwirio';
			b.innerHTML = '<li id=gwirio-neidio>Anwybyddu<li id=gwirio-dysgu>Dysgu<li id=gwirio-nol>Nôl<li id=gwirio-nesaf>Nesaf';
			this.msg = d.appendChild(n);
			this.ol = d.appendChild(o);
			d.appendChild(b);
			this.menu = el.appendChild(d);
		}
		return this.menu;
	}

	dangos(el) {
		this.open && this.cau();
		const data = this.getData(el);
		el.scrollIntoViewIfNeeded ? el.scrollIntoViewIfNeeded(true) : el.scrollIntoView({block:'center',behavior:'smooth'});
		this.edit.classList.add('gwirydd-agored');
		this.menu.className = data.math;
		this.menu.style.top = (el.offsetTop + el.offsetHeight - this.edit.scrollTop) + 'px';
		this.menu.style.left = Math.min(
			el.offsetLeft,
			(parseInt(getComputedStyle(this.edit).marginLeft) || 0) + this.edit.offsetWidth - this.menu.offsetWidth
		) + 'px';
		this.msg.textContent = this.atalnodi(data.neges);
		while (this.ol.lastChild) {
			this.ol.removeChild(this.ol.lastChild);
		}
		for (let o of data.dewis) {
			const li = document.createElement('li');
			li.textContent = o;
			this.ol.appendChild(li);
		}
		if (this.open) this.open.classList.remove('gwall-dangos');
		el.classList.add('gwall-dangos');
		if (this.menu.offsetTop + this.menu.offsetHeight > this.edit.offsetHeight){
			this.edit.style.marginBottom = (this.menu.offsetTop + this.menu.offsetHeight - this.edit.offsetHeight) + 'px';
		}
		this.open = el;
	}

	cau() {
		if (this.open) this.open.classList.remove('gwall-dangos');
		if (this.edit) {
			this.edit.classList.remove('gwirydd-agored');
			this.edit.style.marginBottom = null;
		}
		if (this.menu) this.menu.className = '';
		this.open = false;
	}

	gwirio(root) {
		if (!this.edit) this.gosod(root);
		this.edit.dispatchEvent(new Event('gwirio'));
		this.edit.setAttribute('spellcheck', 'false');
		this.gwirioEto = false;
		const walker = document.createTreeWalker(root || this.edit, NodeFilter.SHOW_ALL, {
			acceptNode: function(node) {
				if (['STYLE', 'SCRIPT'].includes(node.nodeName) || node.id == 'gwirio')
					return NodeFilter.FILTER_REJECT;
				if (node.nodeType !== 3)
					return NodeFilter.FILTER_SKIP;
				return NodeFilter.FILTER_ACCEPT;
			}
		});
		this.textnodes = [];
		let node, text = '';
		while (node = walker.nextNode()) {
			const l = text.length;
			let t = node.textContent + ' ';
			t = t.replace(/\s/g, ' ').replace(/\b[A-Za-z]{3,9}:\/\/\S*/g, function(m){ return ' '.repeat(m.length) });
			this.textnodes.push({start:l, end:l + t.length, node:node});
			text += t;
		}

		//const gwahanu = /([.?!]\s+)(\p{Lu})/ug; // dim i Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1361876
		const gwahanu = /([.?!]\s+)([A-Z\xC0-\xD6\xD8-\xDE\u0100\u0102\u0104\u0106\u0108\u010A\u010C\u010E\u0110\u0112\u0114\u0116\u0118\u011A\u011C\u011E\u0120\u0122\u0124\u0126\u0128\u012A\u012C\u012E\u0130\u0132\u0134\u0136\u0139\u013B\u013D\u013F\u0141\u0143\u0145\u0147\u014A\u014C\u014E\u0150\u0152\u0154\u0156\u0158\u015A\u015C\u015E\u0160\u0162\u0164\u0166\u0168\u016A\u016C\u016E\u0170\u0172\u0174\u0176\u0178\u0179\u017B\u017D])/ug;
		const brawddegau = text.replace(gwahanu, "$1\n$2").split("\n");
		const darnau = [];
		let darn = '', offset = 0;
		for (let b of brawddegau) {
			if (darn.length + b.length > this.maxRequest) {
				darnau.push( this.gwirioDarn(darn, offset) );
				offset += darn.length;
				darn = '';
			}
			darn += b;
		}
		darnau.push( this.gwirioDarn(darn, offset) );
		Promise.all( darnau ).then(() => {
			delete this.textnodes;
		});
	}

	gwirioDarn(text, offset) {
		return this.api(text).then(res => {
			if (res.success && res.result) {
				const l = this.textnodes.length;
				let startNode = 0;
				res.result.sort((a, b) => { return a.start < b.start ? 1 : -1 });
				for (let r of res.result) {
					r.start += offset;
					r.end = r.start + r.length;
					const range = document.createRange();
					try {
						for (let i = startNode; i<l; i++) {
							const n = this.textnodes[i];
							if (r.start >= n.start && r.start < n.end) {
								range.setStart(n.node, r.start - n.start);
							}
							if (r.end >= n.start && r.end < n.end) {
								range.setEnd(n.node, r.start + r.length - n.start);
								break;
							}
						}
					}
					catch(e) {
						// Cywiriadau yn gorlapio
						// Ar ôl gorffen gwirio’r cynnwys, bydd angen ei wirio eto
						console.info('Gwiriad yn gorlapio', r);
						this.gwirioEto = true;
						continue;
					}
					const text = range.toString();
					if (!this.anwybyddwyd(text)) {
						const el = document.createElement('span'),
							data = {neges:r.message, dewis:r.suggestions, text:text, math:r.isSpelling ? 'sillafu' : 'gramadeg'};
						el.className = 'gwall-gwirio gwall-' + data.math;
						this.setData(el, data);
						el.appendChild(range.extractContents());
						range.insertNode(el);
					}
				}
			}
			else if (res.errors) {
				alert(res.errors[0]);
				console.warn(`Gwall API gwirio: ${res.errors[0]}`);
			}
			if (this.diweddaru()) this.edit.classList.add('gwirydd-gwirio');
		});
	}

	api(t){

		const body = new URLSearchParams();
		body.set('api_key', this.apiKey);
		body.set('lang', this.lang);
		body.set('max_errors', 0);
		body.set('text', t);

		return fetch(this.apiUrl, {
			method: 'POST',
			referrerPolicy: 'origin',
			body
		}).then(r => {
			if (!r.ok) {
				console.warn(`Gwall API gwirio: ${r.status} ${r.statusText}`);
				return {};
			}
			this.apiLimit = r.headers.get('X-Ratelimit-Limit');
			this.apiRemaining = r.headers.get('X-Ratelimit-Remaining');
			this.apiReset = r.headers.get('X-Ratelimit-Reset');
			if (this.apiLimit) {
				console.info(`Statws API gwirio: ${this.apiRemaining}/${this.apiLimit} cais ar ôl cyn ${new Date(this.apiReset * 1000).toLocaleString()} [HTTP ${r.status}]`);
			}
			return r.json();
		});

	}

}