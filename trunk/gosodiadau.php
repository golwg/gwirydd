<?php
/* ===================================================================

Rhyngwyneb HTML i’r API Cysill a ddarperir gan Brifysgol Bangor.
Manylion, a sut i gael allwedd API, yma: https://api.techiaith.org/cy/

Datblygwyd gan Iwan Standley ar gyfer cynllun cyhoeddi cymunedol Bro360.

Hawlfraint: Golwg Cyf <gwefan@golwg.com> https://golwg.cymru

Mae’r ffeil hwn yn cyflwyno rhyngwyneb yng ngosodiadau WordPress.

=================================================================== */

wp_enqueue_script( 'gwirydd', plugins_url('/gwirydd.js', __FILE__), false, GWIRYDD_FERSIWN, true);

?>
<div class="wrap">

	<h1>Gosodiadau Gwirydd Sillafu</h1>

	<div class="notice notice-info">
	<p>Diolch i <a href="https://www.bangor.ac.uk/canolfanbedwyr/">Brifysgol Bangor</a>
	am ddarparu’r adnodd Cysill Ar-lein sy’n sail i’r gwirydd sillafu hwn.</p>
	<p><b>Nodwch y wybodaeth bwysig hon ganddyn nhw:</b></p>
	<p><i>Hoffem i chi fod yn ymwybodol, wrth i chi ddefnyddio Cysill
	Ar-lein, y bydd eich testunau yn cael eu danfon at ein gweinydd(ion)
	er mwyn gwirio'r sillafu a'r gramadeg. Mae Cysill Ar-lein yn cadw
	copïau o destunau sy'n cael eu gyrru ato er mwyn eu cynnwys o fewn
	corpws o destunau. Bydd y corpws hwnnw yn cyfrannu at ymchwil
	academaidd, gwelliannau yn Cysill a datblygiad technolegau iaith yn
	y dyfodol. Peidiwch â rhoi unrhyw ddeunydd cyfrinachol neu sensitif
	i mewn.</i></p>
	<p>Darllenwch y <a href="https://www.cysgliad.com/cysill/arlein/">telerau
	llawn ar wefan Cysgliad</a>.<p>
	</div>

	<?php if ( gwirydd_rhwydwaith() ? is_network_admin() : ( !is_network_admin() && current_user_can('activate_plugins') ) ) { ?>

		<h2>Cychwyn arni</h2>

		<p><a href="https://api.techiaith.org/cy/account/signup/"
		target="techiaith">Cofrestrwch gyda’r Porth Technolegau Iaith</a> i
		dderbyn eich <a href="https://api.techiaith.org/cy/apis/cysillapi/"
		target="techiaith">allwedd API ar gyfer Cysill Ar-lein</a>.</p>

		<form action="<?php echo is_network_admin() ? 'edit.php?action=gwirydd' : 'options.php'; ?>" method="post">

		<?php
			settings_fields( 'gwirydd' );
			do_settings_sections( 'gwirydd' );
			submit_button();
		?>

		</form>
		<hr>

	<?php } elseif ( gwirydd_rhwydwaith() && !is_network_admin() && current_user_can( 'manage_network_plugins' ) ) { ?>

		<p>Gallwch osod allwedd API yn y <a href="<?php echo network_admin_url('settings.php?page=gwirydd'); ?>">gosodiadau rhwydwaith</a>.</p>

	<?php } ?>

	<h2>Geiriadur personol</h2>
	<p>Dyma’r geiriau rydych chi wedi dewis dysgu wrth wirio testun. Maen nhw’n cael eu cadw yn eich porydd.</p>

	<p><select id="gwirydd-geiriadur" multiple size="10" style="min-width:20em"></select></p>
	<p><input type="button" id="gwirydd-dileu" class="button button-primary" value="Dileu gair"></p>

	<script>
	window.addEventListener('DOMContentLoaded', function(){
	
		const gwirydd = new Gwirydd(),
			geiriadur = gwirydd.darllen(),
			s = document.getElementById('gwirydd-geiriadur'),
			b = document.getElementById('gwirydd-dileu');
	
		for (let g of geiriadur.sort()) {
			const o = document.createElement('option');
			o.label = g;
			s.appendChild(o);
		}
	
		b.addEventListener('click', function(e){
			while (s.selectedOptions.length) {
				gwirydd.anghofio(s.selectedOptions[0].label);
				s.selectedOptions[0].remove();
			}
		});
	
	});
	</script>

</div>
