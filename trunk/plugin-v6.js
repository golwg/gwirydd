/* ===================================================================

Rhyngwyneb HTML i’r API Cysill a ddarperir gan Brifysgol Bangor.
Manylion, a sut i gael allwedd API, yma: https://api.techiaith.org/cy/

Datblygwyd gan Iwan Standley ar gyfer cynllun cyhoeddi cymunedol Bro360.

Hawlfraint: Golwg Cyf <gwefan@golwg.com> https://golwg.cymru

Mae’r ffeil hwn yn ategyn ar gyfer TinyMCE sy’n llwytho’r gwirydd i’r golygydd.
Mae modd ei ddefnyddio gyda TinyMCE yn annibynnol o WordPress hefyd.

=================================================================== */

'use strict';

(function() {

	const version = 1.3;

	// tinymce.create('tinymce.plugins.gwirydd', {
	tinymce.PluginManager.add('gwirydd', (editor, url) => {

		editor.options.register('gwirydd', {
  			processor: 'object',
  			default: {}
		});
		const options = editor.options.get('gwirydd');

		editor.ui.registry.addIcon('gwirydd', '<div class="gwirydd-eicon"><svg width="24" height="24"><path d="m12.6 21h-1.9l-4.7-6.6 1.9-1.8 3.7 3.5 7.4-8.6 1.9 1.4-8.4 12.1zm-9.6-14.2c0 2.5 1.6 3.7 3.6 3.7s1.7-.4 2.3-1.1l-1.1-1.2c-.3.3-.7.6-1.2.6-1 0-1.7-.8-1.7-2.1s.7-2.1 1.6-2.1.8.2 1.2.5l1.1-1.2c-.5-.5-1.3-.9-2.2-.9-2 0-3.6 1.4-3.6 3.8zm8.4 3.6h1.9v-2.4l2.3-4.9h-2l-.6 1.6c-.2.6-.4 1.1-.6 1.7-.2-.6-.4-1.1-.6-1.7l-.6-1.6h-2.1l2.3 4.9z"/></svg></div>');
		editor.ui.registry.addIcon('acenion', '<svg width="24" height="24"><path d="m3.5 8.3h2.8l1.4 6.1c.2 1.1.4 2.2.6 3.4.2-1.1.5-2.3.8-3.4l1.6-6.1h2.6l1.6 6.1c.3 1.1.5 2.2.8 3.4.2-1.1.4-2.2.6-3.4l1.4-6.1h2.6l-3 11.7h-3.3l-1.4-5.5c-.3-1.1-.5-2.2-.7-3.4-.2 1.2-.4 2.3-.7 3.4l-1.4 5.4h-3.2l-3.2-11.7zm7.1-5.3h2.8l2.9 4.2h-2.4l-1.8-2.5h-.1l-1.8 2.5h-2.4l2.9-4.2z"/></svg>');
		editor.ui.registry.addIcon('prysur', '<svg width="24" height="24"><path d="m7.7 11c.4-2 2.2-3.5 4.3-3.5s2.7.7 3.5 1.8l1.7-2c-1.2-1.4-3.1-2.3-5.2-2.3-3.5 0-6.4 2.6-6.9 6h-2.1l3.5 4 3.5-4zm9.8-2-3.5 4h2.3c-.5 2-2.2 3.5-4.3 3.5s-2.7-.7-3.5-1.8l-1.7 1.9c1.2 1.5 3.1 2.4 5.2 2.4 3.5 0 6.4-2.6 6.9-6h2.1z"><animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 12 12" to="-360 12 12" dur="2s" repeatCount="indefinite"/></path></svg>');

		let gwirydd, botwm, rhif = false;
		try {
			new Function('(y => y)'); // cadarnhau ES6
			gwirydd = new Gwirydd(options.allwedd || window.gwirydd.allwedd);
		}
		catch (e) {
			console.warn('Dydi’r gwirydd ddim yn gweithio yn y porydd hwn.');
		}

		var gwirioHandler = function(e) {
			if (!botwm) return;
			botwm.isEnabled(false);
			botwm.setIcon('prysur');
		}

		var gwallHandler = function(e) {
			if (!botwm) return;
			botwm.isEnabled(true);
			if (e.detail === 0) {
				if (rhif) {
					//if (eicon.nextElementSibling) eicon.nextElementSibling.remove();
					//botwm.setText(false);
					rhif.remove();
					rhif = false;
				}
				else {
					alert('Wedi gorffen gwirio.');
				}
				botwm.setIcon('gwirydd');
				botwm.setActive(false);
			}
			else {
				if (!rhif) {
					botwm.setActive(true);
					botwm.setIcon('gwirydd');
					rhif = document.createElement('b');
					rhif.className = 'gwirydd-rhif';
					const eicon = editor.getContainer().querySelector('.gwirydd-eicon');
					if (eicon) eicon.insertAdjacentElement('beforeend', rhif);
				}
				rhif.textContent = e.detail;
			}
		};

		editor.ui.registry.addToggleButton( 'gwirydd', {
			type: 'toggleButton',
			//text: 'Gwirydd Sillafu',
			tooltip: gwirydd ?
				'Dangos awgrymiadau sillafu a gramadeg Cymraeg.' :
				'Dydi’r gwirydd ddim ar gael ar hyn o bryd.',
			icon: 'gwirydd',
			onSetup: api => {
				botwm = api;
				if (!gwirydd) api.setEnabled(false);
			},
			onAction: api => {
				if (!options?.allwedd && !window.gwirydd) {
					alert('Mae angen rhoi’r allwedd API yn y Gosodiadau.');
					return;
				}
				var body = editor.getBody();
				if (api.isActive()) {
					api.setActive(false);
					gwirydd.clirio();
				}
				else {
					api.setActive(true);
					gwirydd.addListener(body, 'gwirio', gwirioHandler);
					gwirydd.addListener(body, 'gwallau', gwallHandler);
					const stad = editor.isDirty();
					gwirydd.gwirio(body);
					editor.setDirty(stad);
				}
			},
		});

		editor.on( 'PreProcess', function(e) {
			for (var el of e.node.querySelectorAll('.gwall-gwirio')) {
				el.replaceWith(...el.childNodes);
			}
			var g = e.node.querySelector('#gwirio');
			if (g) g.remove();
		});

		editor.on( 'hide', function(e) {
			if (e.type == 'hide' && editor.isHidden()) {
				gwirydd.clirio();
			}
		});

		editor.ui.registry.addButton('acenion', {
			title: 'Acenion Cymraeg',
			tooltip: 'Teipio toeon bach ac acenion eraill.',
			icon: 'acenion',
			onAction: function(e) {
				ffenestAcenion(editor);
			},
		});

		// CSS i’r blwch golygu
		editor.contentCSS.push(url + '/golygydd.css?v=' + version);

		// CSS i’r golygydd
		var link = document.createElement('link');
		link.href = url + '/gwirydd.css?v=' + version;
		link.rel = 'stylesheet';
		document.head.appendChild(link);

		// },
		// createControl: function(n, cm) {
		// 	return null;
		// },
		// getInfo: function() {
		// 	return {
		// 		longname: 'Gwirydd Sillafu',
		//
		// 		authorurl: 'https://stanno.cymru',
		// 		infourl: '',
		//
		// 	};
		// // }

		return {
			getMetadata: () => ({
				name: 'Gwirydd Sillafu',
				author: 'Iwan Stanno',
				url: 'https://stanno.cymru',
				version
			})
		};

	});

	function ffenestAcenion(editor) {
		var win;
		var acenion = 'ˆ¨´`˜';
		var chars = ['âêîôûŵŷ', 'äëïöüẅÿ', 'áéíóúẃý', 'àèìòùẁỳ', 'ãñõåøçæ'];
		var i = 0;
		var tabl = '<table class="gwirydd-acenion">';
		for (var r of chars) {
			var tr1 = '<tr class="' + (i++ ? 'cuddio ' : '') + 'rhes colofn-' + i + '">',
				tr2 = tr1;
			for (var c of r.split('')) {
				tr1 += '<td><div>' + c + '</div></td>';
				tr2 += '<td><div>' + c.toUpperCase() + '</div></td>';
			}
			tabl += tr1 + '</tr>' + tr2 + '</tr>';
		}
		tabl += '</table><table class="gwirydd-acenion"><tr>';
		i = 0;
		for (var a of acenion.split('')) {
			tabl += '<th' + (i++ ? '' : ' class="dangos"') + ' data-colofn="' + i + '"><div>'+a+'</div></th>';
		}
		tabl += '</tr></table>';
		var panel = {
			type: 'htmlpanel',
			html: tabl,
			onAction: function (e) {
				var cell = e.target.closest('td,th');
				var ct = e.currentTarget;
				if (!cell) {
					return;
				} else if (cell.nodeName == 'TD') {
					editor.insertContent(cell.textContent);
					win.close();
				} else if (cell.classList.contains('dangos')) {
					return;
				} else {
					var i = cell.dataset.colofn;
					for (var el of ct.querySelectorAll('.rhes:not(.colofn-' + i + ')')) {
						el.classList.add('cuddio');
					}
					for (var el of ct.querySelectorAll('.colofn-' + i)) {
						el.classList.remove('cuddio');
					}
					for (var el of ct.querySelectorAll('.dangos')) {
						el.classList.remove('dangos');
					}
					cell.classList.add('dangos');
				}
			},
		};
		win = editor.windowManager.open({
			title: 'Mewnosod acenion',
			spacing: 10,
			padding: 10,
			body: {
				type: 'panel',
				items: [panel]
			},
			buttons: [{
				type: 'cancel',
				buttonType: 'primary',
				text: 'Cau',
				// onclick: function() {
				// 	win.close();
				// }
			}]
		});
	};


	// tinymce.PluginManager.add('gwirydd', tinymce.plugins.gwirydd);

})();
