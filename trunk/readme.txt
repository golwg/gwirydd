=== Gwirydd Sillafu Cymraeg ===
Contributors: stanno
Tags: spelling, grammar, welsh, sillafu, gramadeg, cymraeg
Requires at least: 5.0
Tested up to: 5.6.2
Requires PHP: 5.6
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Gwirio sillafu a gramadeg gydag adnodd Cysill Prifysgol Bangor. Welsh-language spelling and grammar checker.

== Description ==

Dangoswch awgrymiadau sillafu a gramadeg wrth gyhoeddi yn y Gymraeg gyda WordPress.

Datblygwyd yr ategyn hwn ar gyfer [gwasanaeth newyddion a materion cyfoes Golwg360](https://golwg.360.cymru) a [chynllun cyhoeddi cymunedol Bro360](https://bro.360.cymru) gan [Iwan Standley](https://stanno.cymru) ar ran [Golwg](https://golwg.cymru).

Mae’n defnyddio’r gwasanaeth Cysill Ar-lein a ddarperir gan [Brifysgol Bangor](https://www.bangor.ac.uk/canolfanbedwyr), er mwyn cynnig cywiriadau a gwelliannau wrth i chi ysgrifennu cofnodion o fewn golygydd WordPress. Bydd angen i chi [gofrestru i greu cyfrif am ddim](https://api.techiaith.org/cy/account/signup/) er mwyn defnyddio’r gwasanaeth (mae rhagor o fanylion yn yr ategyn).

Mae hefyd yn cynnig ffordd hawdd o osod toeon bach ac acenion eraill yn eich cofnod.

Mae **Golwg** yn [falch o rannu’r adnodd hwn](https://bro.360.cymru/2021/gwirydd-sillafu-gramadeg-cymraeg/) gyda’r gymuned gyhoeddi Cymraeg. Mae croeso i chi [gynnig gwelliannau ac ailddefnyddio’r cod](https://gitlab.com/golwg/gwirydd).

*This Welsh-language spelling and grammar checker is provided by **Golwg** using Bangor University’s Cysill API.*

== Frequently Asked Questions ==

= Ydi’r gwirydd yn gweithio gyda HTML a thestun wedi ei fformatio? =

Ydi, os yw’r fformatio’n weddol syml. 

= Ydi’r gwirydd yn gweithio gyda’r golygydd blociau? =

Ar hyn o bryd, mae’r gwirydd yn gweithio gyda’r golygydd clasurol, a blociau clasurol o fewn y golygydd blociau. Os ydych chi’n defnyddio’r golygydd blociau yn rheolaidd, ac am wirio testun yn y blociau sylfaenol (hynny yw, nid yr un clasurol), rhowch wybod i ni—byddai’n ddiddorol gwybod os oes galw am hyn.

= Ydi’r gwirydd yn gweithio gyda rhwydwaith WordPress aml-safle? =

Ydi. Gallwch fywiogi’r ategyn ar lefel y rhwydwaith, yna gosod yr allwedd API unwaith ar gyfer yr holl safleoedd yn y gosodiadau rhwydwaith. Neu, mae modd bywiogi‘r ategyn ar lefel safleoedd unigol a gosod allweddau API gwahanol iddynt.

= Sut ydw i’n cofrestru i dderbyn allwedd API? =

Er mwyn defnyddio’r adnodd sillafu a gramadeg, bydd angen i chi greu [cyfrif ar wefan Porth Technolegau Iaith](https://api.techiaith.org/cy/account/signup/) ac yna creu [allwedd API ar gyfer y gwasanaeth Cysill Ar-lein](https://api.techiaith.org/cy/apis/cysillapi/).

Awgrymwn i chi *beidio* â dewis yr opsiwn “Bydd yr allwedd API yn cael ei defnyddio ar wefan” gan nad yw’r cyfyngiad hwn yn gweithio gyda phob porydd.

= Ydi fy ngwybodaeth bersonol yn ddiogel? =

Nodwch y wybodaeth bwysig hon ynglŷn â gwasanaeth Cysill Ar-lein:

> Hoffem i chi fod yn ymwybodol, wrth i chi ddefnyddio Cysill Ar-lein, y bydd eich testunau yn cael eu danfon at ein gweinydd(ion) er mwyn gwirio'r sillafu a'r gramadeg. Mae Cysill Ar-lein yn cadw copïau o destunau sy'n cael eu gyrru ato er mwyn eu cynnwys o fewn corpws o destunau. Bydd y corpws hwnnw yn cyfrannu at ymchwil academaidd, gwelliannau yn Cysill a datblygiad technolegau iaith yn y dyfodol. Peidiwch â rhoi unrhyw ddeunydd cyfrinachol neu sensitif i mewn.

== Screenshots ==

1. Mae’r ategyn yn ychwanegu dau fotwm i’r golygydd cofnodion.
2. Defnyddiwch y botwm gwirydd i ddangos awgrymiadau sillafu a gramadeg.
3. Mae hefyd yn cynnig ffordd syml o osod llythrennau gyda thoeon bach ac acenion eraill.
4. Gallwch ddefnyddio’r gwirydd o fewn y bloc clasurol yn y golygydd blociau newydd.

== Changelog ==

= 1.3 =
* Mân-newidiadau.

= 1.2 =
* Y fersiwn gyhoeddus gyntaf.
