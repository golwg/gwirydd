<?php
/**
 * Plugin Name: Gwirydd Sillafu Cymraeg
 * Plugin URI:  https://bro.360.cymru/2021/gwirydd-sillafu-gramadeg-cymraeg/
 * Description: Gwirio sillafu a gramadeg gydag adnodd Cysill Prifysgol Bangor.
 * Author: Golwg
 * Author URI: https://golwg.cymru
 * Version: 1.3
 * Text Domain: gwirydd
 */

/* ===================================================================

Rhyngwyneb HTML i’r API Cysill a ddarperir gan Brifysgol Bangor.
Manylion, a sut i gael allwedd API, yma: https://api.techiaith.org/cy/

Datblygwyd gan Iwan Standley ar gyfer cynllun cyhoeddi cymunedol Bro360.

Hawlfraint: Golwg Cyf <gwefan@golwg.com> https://golwg.cymru

Dyma’r bachau sy’n llwytho’r ategyn i fewn i WordPress.

=================================================================== */

if (!defined('ABSPATH')) exit;

define('GWIRYDD_FERSIWN', '1.3');

require_once('cymraeg.php');

add_action( 'wp_loaded', function() {

	if ( !( current_user_can('edit_posts')  || current_user_can('edit_pages') )
		|| get_user_option('rich_editing') !== 'true' || !gwirydd_allwedd()
	) {
		return;
	}

	add_filter( 'mce_external_plugins', function($plugins) {
		$plugins['gwirydd'] = plugins_url('/plugin.js?v=' . GWIRYDD_FERSIWN, __FILE__);
		return $plugins;
	});

	add_filter( 'mce_buttons', function($buttons) {
		array_splice($buttons, array_search('wp_adv', $buttons) ?: count($buttons), 0, ['gwirydd', 'acenion']);
		return $buttons;
	});

	add_filter( 'tiny_mce_before_init', function($mceInit) {
		$mceInit['gwirydd'] = json_encode([
			'allwedd' => gwirydd_allwedd(),
		]);
		return $mceInit;
	});

	add_action( 'wp_enqueue_editor', 'gwirydd_sgript' );

});

add_action( 'admin_init', function() {
	global $pagenow;

	$plugin = plugin_basename( __FILE__ );

	register_setting( 'gwirydd', 'gwirydd_api', [ 'type' => 'string', 'default' => '' ] );
	// register_setting( 'gwirydd', 'gwirydd_vocab_api', [ 'type' => 'string', 'default' => '' ] );
	// register_setting( 'gwirydd', 'gwirydd_vocab_lle', [ 'type' => 'string', 'default' => '' ] );

	add_filter( "plugin_action_links_{$plugin}" , function($links) {
		$links[] = '<a href="' . admin_url('options-general.php?page=gwirydd') . '">' . __( 'Settings' ) . '</a>';
		return $links;
	});

	add_filter( "network_admin_plugin_action_links_{$plugin}", function($links) {
		$links[] = '<a href="' . network_admin_url('settings.php?page=gwirydd') . '">' . __( 'Settings' ) . '</a>';
		return $links;
	});

	add_settings_section(
		'gwirydd',
		'',
		'',
		'gwirydd'
	);

	add_settings_field(
		'gwirydd_api',
		'Allwedd API',
		function() {
			echo '<input type="text" id="gwirydd_api" name="gwirydd_api" value="' . esc_attr(gwirydd_allwedd()) . '" class="regular-text">';
		},
		'gwirydd',
		'gwirydd',
		[
			'label_for' => 'gwirydd_api'
		]
	);

	if ( in_array( $pagenow, ['post.php', 'post-new.php'] )
		&& !gwirydd_allwedd()
		&& current_user_can( 'activate_plugins' )) {

		add_action( 'admin_notices', function() {
			$url = gwirydd_rhwydwaith() ? network_admin_url('settings.php?page=gwirydd') : admin_url('options-general.php?page=gwirydd');
			echo '<div class="notice notice-warning"><p>Mae angen <a href="' . $url
			. '">gosod eich allwedd API</a> er mwyn defnyddio’r gwirydd sillafu.</p></div>';
		});

	}

});

add_action( 'admin_menu', function () {

	add_options_page( 'Gosodiadau Gwirydd Sillafu', 'Gwirydd Sillafu', 'edit_posts', 'gwirydd', function(){
		require_once(__DIR__ . '/gosodiadau.php');
	});

});

add_action( 'network_admin_menu', function() {

	if ( gwirydd_rhwydwaith() ) {
		add_submenu_page( 'settings.php', 'Gosodiadau Gwirydd Sillafu', 'Gwirydd Sillafu', 'edit_posts', 'gwirydd', function(){
			require_once(__DIR__ . '/gosodiadau.php');
		});
	}

});

add_action( 'network_admin_edit_gwirydd', function() {

	check_admin_referer('gwirydd-options');

	if ( !current_user_can( 'manage_network_plugins' ) ) {
		wp_die( __( 'You need a higher level of permission.' ) );
	}

	$allwedd = isset( $_REQUEST['gwirydd_api'] ) ? sanitize_key( $_REQUEST['gwirydd_api'] ) : '';
	update_site_option('gwirydd_api', $allwedd);

	wp_redirect( network_admin_url('settings.php?page=gwirydd&updated=true') );
	exit();

});

add_action( 'wp_ajax_geiriadur_gwirydd', function() {

	$gwirydd = empty($_POST['gwirydd']) ? false : sanitize_text_field($_POST['gwirydd']);
	$gair = empty($_POST['gair']) ? false : sanitize_text_field($_POST['gair']);
	$geiriau = (array)get_user_option('gwirydd_geiriadur', []);

	switch ($gwirydd) {
		case 'dysgu':
			if (!in_array($gair, $geiriau)) {
				$geiriau[] = $gair;
				update_option('gwirydd_geiriadur', $geiriau);
			}
			break;
		case 'anghofio':
			$index = array_search($gair, $geiriau);
			if ($index !== false) {
				unset($geiriau[$index]);
				update_option('gwirydd_geiriadur', $geiriau);
			}
			break;
	}

	wp_die();

});

function gwirydd_rhwydwaith() {
	if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
	    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	}
	return is_plugin_active_for_network( plugin_basename( __FILE__ ) );
}

function gwirydd_allwedd() {
	return gwirydd_rhwydwaith() ? get_site_option('gwirydd_api', '') : get_option('gwirydd_api', '');
}

function gwirydd_sgript() {
	wp_enqueue_script( 'gwirydd', plugins_url('/gwirydd.js', __FILE__), false, GWIRYDD_FERSIWN, true);
}