# Gwirydd sillafu Cymraeg i HTML

Gwirio sillafu a gramadeg o fewn WordPress, TinyMCE, neu unrhyw dalp o HTML, gydag adnodd Cysill Prifysgol Bangor.

Datblygwyd ar gyfer gwasanaeth newyddion a materion cyfoes [Golwg360](https://golwg.360.cymru) a chynllun cyhoeddi cymunedol [Bro360](https://bro.360.cymru) gan [Iwan Standley](https://stanno.cymru).

Mae’n defnyddio’r gwasanaeth Cysill Ar-lein a ddarperir gan [Brifysgol Bangor](https://www.bangor.ac.uk/canolfanbedwyr). Bydd angen i chi [gofrestru i greu cyfrif am ddim](https://api.techiaith.org/cy/account/signup/) er mwyn defnyddio’r gwasanaeth.

Mae [Golwg](https://golwg.cymru) yn falch o rannu’r adnodd hwn gyda’r gymuned gyhoeddi Cymraeg.

# Welsh spelling and grammar checker for HTML

*Welsh-language spelling and grammar checker for WordPress, TinyMCE, or arbitrary HTML, via Bangor University’s Cysill API.*

## Defnyddio o fewn WordPress

Y ffordd hawsaf o ychwanegu’r ategyn i WordPress yw ei lwytho o’r [gronfa ategion](https://cy.wordpress.org/plugins/gwirydd/) neu ewch i Ategion yn eich Bwrdd Rheoli, dewis Ychwanegu, a chwilio am “Gwirydd”.

Mae’r fersiynau a ryddhawyd drwy WordPress ar gael o’u [cronfa SVN](https://plugins.svn.wordpress.org/gwirydd/) hefyd.

## Defnyddio gyda TinyMCE

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <title>Gwirydd TinyMCE</title>
    </head>
    <body>
        <textarea id="golygydd">
            &lt;p&gt;Testun &lt;b&gt;cyfoethog&lt;/b&gt; gwalllus&lt;/p&gt;
        </textarea>
        <script src="/path/to/tinymce.js"></script>
        <script src="/path/to/gwirydd/gwirydd.js"></script>
        <script>
        tinymce.init({
            selector: '#golygydd',
            language: 'cy',
            toolbar: 'undo redo | bold italic | gwirydd acenion',
            element_format: 'html',
            external_plugins: {
                gwirydd: '/path/to/gwirydd/plugin.js'
            },
            gwirydd: {
                allwedd: 'XXXX-ALLWEDD-API-XXXX'
            }
        });
        </script>
    </body>
    </html>

## Defnyddio gyda HTML/Javascript

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <title>Gwirydd</title>
        <link rel="stylesheet" href="golygydd.css">
    </head>
    <body>
        <div id="golygydd">
            <p>Testun <b>cyfoethog</b> gwalllus</p>
        </div>
        <button id="cychwyn">Cychwyn</button>
        <button id="clirio">Clirio</button>
        <script src="gwirydd.js"></script>
        <script>
            const el = document.querySelector('#golygydd');
            const gwirydd = new Gwirydd('XXXX-ALLWEDD-API-XXXX');
    
            gwirydd.addListener(el, 'gwirio', e => {
                console.log('Wrthi’n prosesu…');
            });

            gwirydd.addListener(el, 'gwallau', e => {
                console.log('Wedi canfod gwallau');
            });

            document.querySelector('#cychwyn').addEventListener('click', e => {
                e.preventDefault();
                gwirydd.gwirio(el);
            });

            document.querySelector('#clirio').addEventListener('click', e => {
                e.preventDefault();
                gwirydd.clirio();
            });
        </script>
    </body>
    </html>

## Cyfrannu a chysylltu

[Darllenwch ragor am yr adnodd hwn ar flog Bro360](https://bro.360.cymru/2021/gwirydd-sillafu-gramadeg-cymraeg/).
 
Cysylltwch gyda [@stanno](https://twitter.com/stanno) ar Twitter neu drwy [ffurflen gysylltu Golwg](https://360.cymru/cysylltu/).

Croeso i chi gysylltu gydag adborth a gwelliannau posib (ond dydi hi ddim yn bosib i ni gynnig cymorth technegol manwl, yn anffodus).
